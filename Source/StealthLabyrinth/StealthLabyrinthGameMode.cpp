// Copyright Epic Games, Inc. All Rights Reserved.

#include "StealthLabyrinthGameMode.h"
#include "StealthLabyrinthCharacter.h"
#include "UObject/ConstructorHelpers.h"

AStealthLabyrinthGameMode::AStealthLabyrinthGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
