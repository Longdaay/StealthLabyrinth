// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StealthLabyrinthGameMode.generated.h"

UCLASS(minimalapi)
class AStealthLabyrinthGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AStealthLabyrinthGameMode();
};



