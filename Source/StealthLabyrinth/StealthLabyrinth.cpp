// Copyright Epic Games, Inc. All Rights Reserved.

#include "StealthLabyrinth.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, StealthLabyrinth, "StealthLabyrinth" );
 